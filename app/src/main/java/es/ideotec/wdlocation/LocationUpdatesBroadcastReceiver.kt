package es.ideotec.wdlocation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task

private const val TAG = "LUBR"

class LocationUpdatesBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive() context:$context, intent:$intent")
        if (intent.action == "es.ideotec.wdlocation.LocationUpdatesBroadcastReceiver.action.PROCESS_UPDATES") {
            val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            try {
                val task: Task<Location> = fusedLocationClient.lastLocation
                task.addOnCompleteListener { task ->
                    val location: Location = task.result
                    context.openFileOutput("locations.txt", Context.MODE_APPEND or Context.MODE_PRIVATE).use {
                        it.write(System.getProperty("line.separator").toByteArray())
                        it.write("Accuracy: ${location.accuracy}".toByteArray())
                        it.write("Altitude: ${location.altitude}".toByteArray())
                        it.write("Bearing: ${location.bearing}".toByteArray())
                        it.write("BearingAccuracy: ${location.bearingAccuracyDegrees}".toByteArray())
                        it.write("Latitude: ${location.latitude}".toByteArray())
                        it.write("Longitude: ${location.longitude}".toByteArray())
                        it.write("Provider: ${location.provider}".toByteArray())
                        it.write("Speed: ${location.speed}".toByteArray())
                        it.write("SpeedAccuracy: ${location.speedAccuracyMetersPerSecond}".toByteArray())
                        it.write("Time: ${location.time}".toByteArray())
                        it.write("VerticalAccuracy: ${location.verticalAccuracyMeters}".toByteArray())
                        it.write(System.getProperty("line.separator").toByteArray())
                    }
                }
            } catch (permissionRevoked: SecurityException) {
                Log.d(TAG, "SecurityException")
            }
        }
    }
}