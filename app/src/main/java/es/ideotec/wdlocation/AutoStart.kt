package es.ideotec.wdlocation

import android.Manifest
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

private const val TAG = "LUBR: AutoStart"

class AutoStart : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            Log.d(TAG, "Received ACTION_BOOT_COMPLETED.")
            WDLocation(context).startLocationUpdates()
        } else if (intent.action == Intent.ACTION_MY_PACKAGE_REPLACED) {
            Log.d(TAG, "Received ACTION_MY_PACKAGE_REPLACED.")
            WDLocation(context).startLocationUpdates()
        }
    }
}